# wdio-jobla

Cucumber step definitions written with WebdriverIO for test task using JS, WebdriverIO and Cucumber.js. 

# Framework installation

**Install node js**

[Node.js](http://nodejs.org/) version 8 or higher:

- download here https://nodejs.org/en/download/

**Install Java 8**

- download here https://www.java.com/en/download/

**Install node-gyp if needed**

- download here https://github.org/nodejs/node-gyp

**Install xcode if needed**
To install  xcode just run:
```
$ xcode-select --install
```

## Supported versions

[WebdriverIO](https://www.npmjs.com/package/webdriverio):
- 4.x

## Package installation
To install necessary packages just run:
```
$ npm install
```

## How to run the tests
To run your tests just run:
```
$npm test
```

# Configurations

To configure your tests, edit the `wdio.conf.js` file

