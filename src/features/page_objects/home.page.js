const Page = require('./page.js');

class HomePage extends Page {

  get logoHomePage() { return $("//img[@alt='Jobla.co Logo']") }
  get btnSignup() { return $("//div[contains(@to,'/signup')]") }
  get btnLogin() { return $("//div[contains(@to,'/login')]") }
  get btnLanguage() { return $("//div[@class='job-round-btn language-switcher__close-icon']") }
  get btnLanguageUkr() { return $("//a[@href='/uk']") }
  get btnLanguageRus() { return $("//a[contains(text(),'rus')]") }
  get btnLanguageEng() { return $("//a[contains(text(),'eng')]") }
  get btnAcceptCookies() { return $("//button[@class='btn btn-success']") }

  open() {
    super.open('login')
  }

  signup() {
    this.btnSignup.click()
  }
  waitForloginPageToLoad() {
    if (!this.logoHomePage.isVisible()) {
      this.logoHomePage.waitForVisible(10000);
    }
  }

  checkPage(pageName) {
    let pages = {
      home: {
        title: 'Jobla.co | Анонімний пошук роботи для ІТ-спеціалістів'
      },
      signup: {
        title: 'Jobla.co | Реєстрація'
      }
    }
    let page = pages[pageName]
    return browser.getTitle() == page.title
  }

  switchToUkr() {
    this.btnLanguage.waitForVisible()
    this.btnLanguage.click()
    this.btnLanguageUkr.waitForVisible()
    this.btnLanguageUkr.click()
  }
}

module.exports = new HomePage();