const { When } = require('cucumber');
const homePage = require('../features/page_objects/home.page.js');
const signupPage = require('../features/page_objects/signup.page.js');


When('I switch language to ukr', () => {
  homePage.switchToUkr()
})

When('I accept cookies', () => {
  homePage.btnAcceptCookies.click()
})

When('I click on registration button', () => {
  homePage.signup()
})

When('I submit signup form', async () => {
  await signupPage.formSignup.waitForVisible()
  signupPage.signup()
})

When(/I enter ([^"]*) value/, (emailValue) => {
  signupPage.setEmail(emailValue)
})

When(/I enter (?:invalid|valid) email "([^"]*)"/, (emailValue) => {
  signupPage.setEmail(emailValue)
})

When(/I enter (?:invalid|valid) password "([^"]*)"/, (passwordValue) => {
  signupPage.setPassword(passwordValue)
})

When(/I clear ([^"]*) field/, (fieldName) => {
  signupPage.clearField(fieldName)
})

When(/I click ([^"]*) field/, (fieldName) => {
  signupPage.clickField(fieldName)
})

When('I refresh browser tab', () => {
  browser.refresh()
})

