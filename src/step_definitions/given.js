const { Given } = require('cucumber');
const homePage = require('../features/page_objects/home.page.js');


Given('I open the home page', function () {
  browser.deleteCookie().url('./')
  homePage.waitForloginPageToLoad()
})