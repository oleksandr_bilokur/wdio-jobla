const { Then } = require('cucumber');
const homePage = require('../features/page_objects/home.page.js');
const signupPage = require('../features/page_objects/signup.page.js');


Then('account is not created', async () => {
  await signupPage.msgCheckMailboxUkr.waitForVisible(1000, true)
})

Then(/([^"]*) message is shown/, (errMessage) => {
  signupPage[errMessage].waitForVisible(10000, false)
})

Then(/([^"]*) message is not shown/, (errMessage) => {
  signupPage[errMessage].waitForVisible(10000, true)
})

Then(/I expect to be on ([^"]*) page/, (pageName) => {
  browser.waitUntil(() => homePage.checkPage(pageName), 5000, 'expected page should be loaded')
})

Then(/([^"]*) field is present/, (fieldName) => {
  signupPage.checkField(fieldName)
})

Then(/([^"]*) checkbox is active/, async (order) => {
  await signupPage.checkPassActiveOrder(order).waitForVisible(3000, false)
})

Then(/([^"]*) checkbox is inactive/, async (order) => {
  await signupPage.checkPassActiveOrder(order).waitForVisible(3000, true)
})

